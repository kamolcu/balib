# -*- coding: utf-8 -*-
import os
import re
import subprocess

def _get_auth_string(user='root', password=None):
    if user is not None and user.strip() != '':
        cmd='cmd.exe /c mysql -u %s ' % user.strip()
    else:
        return None
    if password is not None and password.strip() != '':
        cmd += '-p'+password.strip() + ' '        
    return cmd
    
def get_all_table_names(user='root', password=None, db_name=None):
    cmd = _get_auth_string(user, password)        
    if db_name is not None and db_name.strip() != '':
        cmd += db_name + ' '
    else:
        return None
    cmd += '-e "show tables"'
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE)
    out,err = process.communicate()
    if err is not None:
        print 'error=',err
    return out.split('\r\n')[1:-1]
        
def get_table_description(user='root', password=None, db_name=None, table_name=None):
    cmd = _get_auth_string(user, password)
    if db_name is not None and db_name.strip() != '':
        cmd += db_name + ' '
    else:
        return None    
    if table_name is not None:
        if type(table_name) == type([]) and len(table_name) > 0:
            cmd += '-e "'
            for table in table_name:
                cmd += 'describe %s;' % table
            cmd+= '"'
        elif type(table_name) == type(''):
            cmd += '-e "describe %s"' % table_name.strip()
    #print cmd
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE)
    out,err = process.communicate()
    if err is not None:
        print 'error=',err
    return out.split('\r\n')[:-1]

def get_create_statement(user='root', password=None, db_name=None, table_name=None):
    cmd = _get_auth_string(user, password)
    if db_name is not None and db_name.strip() != '':
        cmd += db_name + ' '
    else:
        return None    
    if table_name is not None:
        if type(table_name) == type([]) and len(table_name) > 0:
            cmd += '-e "'
            for table in table_name:
                cmd += 'show create table %s;' % table
        elif type(table_name) == type(''):
            cmd += '-e "show create table %s"' % table_name.strip()
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE)
    out,err = process.communicate()
    if err is not None:
        print 'error=',err
    return out.split('\r\n')[1:-1]

def get_foreign_key_info(create_statement=None):
    output=''
    if create_statement is not None and type(create_statement) == type(''):
        match = re.search('CREATE TABLE `(\S*)` [(]', create_statement)    
        table_name = match.group(1)        
        for line in create_statement.split('\\n')[1:]:
            match = re.search('CONSTRAINT \S* FOREIGN KEY \S* REFERENCES `(\S*)` \(`(\S*)`\)', line)
            if match is not None and len(match.groups()) > 0:                        
                another_table = match.group(1)
                foreign_field = match.group(2)
                output += '[' + table_name + ' <==> ' + another_table +'] with foreign key: '+ foreign_field + '\n'
        return output

def get_row_count(user='root', password=None, db_name=None, table_name=None):
    cmd = _get_auth_string(user, password)
    if db_name is not None and db_name.strip() != '':
        cmd += db_name + ' '
    else:
        return None    
    if table_name is not None:
        if type(table_name) == type('') and table_name.strip() != '':
            cmd += '-e "select count(*) from %s"' % table_name.strip()            
            process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE)
            out,err = process.communicate()
            if err is not None:
                print 'error=',err
            return out.split('\r\n')[1]

def main():
    # get all tables
    db_name = 'magento'
    if not os.path.exists(db_name): os.makedirs(db_name)
    all_tables = get_all_table_names(db_name=db_name)
    for table in all_tables:
        file_output = ''
        desc = get_table_description(db_name=db_name,table_name=table)        
        for line in desc:
            file_output += line + '\n'
        create_stmt = get_create_statement(db_name=db_name,table_name=table)        
        file_output += '\n\n'
        for line in create_stmt[0].split('\\n'):
            file_output += line + '\n'
            foreign_key = get_foreign_key_info(create_stmt[0])
            if foreign_key.strip() == '': foreign_key = 'No foreign key'                
            row_count = get_row_count(db_name=db_name, table_name=table)
        file_output += '\n'+foreign_key+'\nrow count = ' + row_count
        with open(db_name + '/' + table + '.txt', 'w') as f_handle:
            f_handle.write(file_output)
        
    print 'Done'
        
main()
    
#print get_create_statement(db_name='magento', table_name='eav_attribute')
#statement=get_create_statement(db_name='magento', table_name='eav_entity')
#print statement
#print get_foreign_key_info(statement[0])
#print get_row_count(db_name='magento', table_name='eav_attribute')
# for table in tables:
#     print get_table_description(db_name='magento', table_name=tables)   
#print get_table_description(db_name='magento', table_name='admin_assert')
        