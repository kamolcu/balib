import os
import re
import sys

def getFileListFromDir(input_dir):
    output = []
    if not os.path.exists(input_dir): return ''
    for item in os.listdir(input_dir):
        result = re.search('[.]feature$', item)
        if result is not None:
            output.append(input_dir + '/' + item)
    return output

def getFeatureFromFile(filePath):
    output = ''
    if not os.path.exists(filePath): return ''
    with open(filePath, 'r') as f_handle:
        for line in f_handle:
            charIndex = line.find('Feature:')
            if charIndex >= 0:
                output = line[charIndex + len('Feature:'):].strip()
                return output

def getScenariosFromFile(filePath):
    output = []
    if not os.path.exists(filePath): return ''
    with open(filePath, 'r') as f_handle:
        for line in f_handle:
            charIndex = line.find('Scenario:')
            if charIndex >= 0:
                output.append(line[charIndex + len('Scenario:'):].strip())
            else:
                charIndex = line.find('Scenario Outline:')
                if charIndex >= 0:
                    output.append(line[charIndex + len('Scenario Outline:'):].strip())
    return output

def getGivenAndWhenThenListFromScenario(scenarioName, filePath):
    output = []
    thenOutput = []
    tmpOutput = ''
    tmpThenOutput = ''
    startProcess = False
    foundThen = False
    foundGivenOrWhen = False
    if not os.path.exists(filePath): return ''
    with open(filePath, 'r') as f_handle:
        for line in f_handle:
            if line.find('Scenario: ' + scenarioName) >= 0 or line.find('Scenario Outline: ' + scenarioName) >= 0:
                startProcess = True
                continue
            if startProcess:
                if line.find('Scenario:') >= 0 or line.find('Scenario Outline:') >= 0:
                    break
                if line.find('Given') >= 0 or line.find('When') >=0:
                    foundGivenOrWhen = True
                    if foundThen and tmpOutput is not '' and tmpThenOutput is not '':
                        output.append(tmpOutput)
                        thenOutput.append(tmpThenOutput)
                        tmpOutput = ''
                        tmpThenOutput = ''
                        foundThen = False
                    tmpOutput += line.strip() + '\n'
                    continue
                elif line.find('Then') >= 0:
                    foundThen = True
                    foundGivenOrWhen = False
                    tmpThenOutput += line.strip() + '\n'
                    continue
                if line.find('And') >= 0 and foundThen:
                    tmpThenOutput += line.strip() + '\n'
                    continue
                elif line.find('And') >= 0 and foundGivenOrWhen:
                    tmpOutput += line.strip() + '\n'
                    continue
        if tmpOutput is not '' and tmpThenOutput is not '':
            output.append(tmpOutput)
            thenOutput.append(tmpThenOutput)
            tmpOutput = ''
            tmpThenOutput = ''
    return output, thenOutput

def makeUATStringFile(featureTxt, scenario, givenWhenList, thenList, outputDir):
    if featureTxt == '': return ''
    if len(scenario) <= 0 or len(givenWhenList) <= 0 or len(thenList) <= 0: return ''
    outfile = featureTxt + '.out.txt'
    outputTxt = ''
    counter = 0
    for givenWhen in givenWhenList:
            outputTxt += featureTxt + '\t' + scenario + '\t"' + givenWhen + '"\t"' + thenList[counter] + '"\n'
            counter += 1
    filePath = outputDir + '/' + outfile
    with open(filePath, 'a') as f_handle:
        f_handle.write(outputTxt)

def main():
    fileList = ''
    if len(sys.argv) > 1:
        fileList = getFileListFromDir(sys.argv[1])
    if fileList != '':
        for item in fileList:
            featureTxt = getFeatureFromFile(item)
            filePath = sys.argv[1] + '/' + featureTxt + '.out.txt'
            if os.path.exists(filePath): os.remove(filePath)
            scenarioList = getScenariosFromFile(item)
            for scenario in scenarioList:
                if len(scenario) > 0:
                    [givenWhenList,thenList] = getGivenAndWhenThenListFromScenario(scenario, item)
                    makeUATStringFile(featureTxt, scenario, givenWhenList, thenList, sys.argv[1])

main()
