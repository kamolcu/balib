# BALib #
This is a python script collection using to facilitate consultant service for BridgeAsia. If it is getting bigger, I will do the properly python module :-P

FILE: database.py contains functionalities below:

get_all_table_names() from a specified mysql database

get_table_description() from specified table name(s)

get_create_statement() from specified table name(s)

get_foreign_key_info() from a specified table name

get_row_count() from a specified table name

### Support only Windows platform
